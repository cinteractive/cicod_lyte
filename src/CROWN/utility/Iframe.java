package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

public class Iframe extends TestBase {

    public static void DoswitchtoframeWhenReady(int frameIndex, int sleepindex) throws IOException, InterruptedException {
        Thread.sleep(sleepindex);
        getDriver().switchTo().frame(frameIndex);
        System.out.println("waited for frame to be present on the page -->" + sleepindex + " milliseconds");
    }

    public static void IframeFinder() {

        List<WebElement> elements = getDriver().findElements(By.tagName("iframe"));
        int numberOfTags = elements.size();
        System.out.println("No. of Iframes on this Web Page are: " + numberOfTags);
    }
}