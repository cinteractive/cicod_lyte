package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FileUpload extends TestBase {

    public static void SaveJsonResponse(String Response, String JsonPath) throws IOException, InterruptedException {
        FileWriter file = new FileWriter(JsonPath);
        file.write(Response);
        file.flush();
        file.close();
    }


    public static void DoFileUpWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(3200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView();", element);
        Actions builder = new Actions(getDriver());
        builder.moveToElement(element).click().build().perform();
    }

    public static void UploadCarouselBanner() throws IOException, InterruptedException, AWTException {

        File Dest = new File("./Config/" + "cicodimg.jpg");
        String extentReportImageqm11 = Dest.getAbsolutePath();

        StringSelection clipBoardPath = new StringSelection(extentReportImageqm11);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipBoardPath, null);
        try {
            //Robot Class
            Robot robot = new Robot();
            //Keyboard Action : CTRL+V

            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);

            Thread.sleep(2500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);

            //Keyboard Action : Enter
            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);

        } catch (StaleElementReferenceException e) {
            System.out.println("An Error Occur but Test will continue");
        }
    }


    public static void UploadXLSX1() throws IOException, InterruptedException, AWTException {

        File Dest = new File("./Config/" +"customer_template1.xlsx");
        String extentReportImageqm11 = Dest.getAbsolutePath();

        StringSelection clipBoardPath = new StringSelection(extentReportImageqm11);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipBoardPath, null);

        try {
            //Robot Class
            Robot robot = new Robot();
            //Keyboard Action : CTRL+V
            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);

            Thread.sleep(2500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);

            //Keyboard Action : Enter
            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);

        } catch (StaleElementReferenceException e) {
            System.out.println("An Error Occur but Test will continue");
        }
    }

    public static void UploadFileImage3MB() throws IOException, InterruptedException, AWTException {

        File Dest = new File(".\\Config\\cicodimg.jpg");
        String extentReportImageqm11 = Dest.getAbsolutePath();

        StringSelection clipBoardPath = new StringSelection(extentReportImageqm11);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipBoardPath, null);

        try {
            //Robot Class
            Robot robot = new Robot();
            //Keyboard Action : CTRL+V
            Thread.sleep(1000);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);

            Thread.sleep(1000);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);

            //Keyboard Action : Enter
            Thread.sleep(500);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);

        } catch (StaleElementReferenceException e) {
            System.out.println("An Error Occur but Test will continue");
        }
    }


    public static void UploadXLSX() throws IOException, InterruptedException, AWTException {

        File Dest = new File("./Config/" + "product_template.xlsx");
        String extentReportImageqm11 = Dest.getAbsolutePath();

        StringSelection clipBoardPath = new StringSelection(extentReportImageqm11);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipBoardPath, null);

        try {
            //Robot Class
            Robot robot = new Robot();
            //Keyboard Action : CTRL+V
            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);

            Thread.sleep(2500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);

            //Keyboard Action : Enter
            Thread.sleep(2500);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);

        } catch (StaleElementReferenceException e) {
            System.out.println("An Error Occur but Test will continue");
        }
    }
}
