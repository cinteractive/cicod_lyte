package CROWN.utility;

import CROWN.Base.TestBase;

import java.io.IOException;

import static CROWN.utility.ActionsClass.DoSendKeysByActionClassWhenReady;
import static CROWN.utility.Utility.DoSendKeysWhenReady;
import static CROWN.utility.Utility.DoclickWhenReady;

public class RavePay extends TestBase {

    public static void RavePay3() throws IOException, InterruptedException {
        Thread.sleep(13000);
        DoSendKeysWhenReady("RavePayCardnumber_XPATH", "Ca_TEXT", "CardNumber_TEXT", 60);
        DoSendKeysWhenReady( "RavePayValidTill_XPATH", "ValidTm_TEXT", "e1_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "RavePayValidTill_XPATH", "e2_TEXT", "V_TEXT", 60);
        DoSendKeysWhenReady("RavPayCVV_XPATH", "CVVd_TEXT", "CVV_TEXT", 60);
        DoclickWhenReady( "fg_XPATH", "P_TEXT", 60);
    }


    public static void RavePay2() throws IOException, InterruptedException {
        Thread.sleep(13000);
        getDriver().switchTo().frame(0);

        DoSendKeysWhenReady("RavePayCardnumber_XPATH", "Ca_TEXT", "CardNumber_TEXT", 60);
        DoSendKeysWhenReady("RavePayValidTill_XPATH", "ValidTm_TEXT", "e1_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "RavePayValidTill_XPATH", "e2_TEXT", "V_TEXT", 60);
        DoSendKeysWhenReady( "RavPayCVV_XPATH", "CVVd_TEXT", "CVV_TEXT", 60);
        DoclickWhenReady( "fg_XPATH", "P_TEXT", 60);
        Thread.sleep(3000);
    }


    public static void RavePay() throws IOException, InterruptedException {
        Thread.sleep(13000);
        getDriver().switchTo().frame(0);

        DoSendKeysWhenReady( "RavePayCardnumber_XPATH", "Ca_TEXT", "CardNumber_TEXT", 60);
        DoSendKeysWhenReady( "RavePayValidTill_XPATH", "ValidTm_TEXT", "e1_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "RavePayValidTill_XPATH", "e2_TEXT", "V_TEXT", 60);
        DoSendKeysWhenReady( "RavPayCVV_XPATH", "CVVd_TEXT", "CVV_TEXT", 60);
        DoclickWhenReady( "fg_XPATH", "P_TEXT", 60);

        DoSendKeysByActionClassWhenReady( "a1_XPATH", "a11_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a2_XPATH", "a21_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady("a3_XPATH", "a31_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a4_XPATH", "a41_TEXT", "E_TEXT", 60);
    }

    public static void RavePay1() throws IOException, InterruptedException {
        Thread.sleep(13000);
        getDriver().switchTo().frame(0);

        DoSendKeysWhenReady( "RavePayCardnumber_XPATH", "Ca_TEXT", "CardNumber_TEXT", 60);
        DoSendKeysWhenReady("RavePayValidTill_XPATH", "ValidTm_TEXT", "e1_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "RavePayValidTill_XPATH", "e2_TEXT", "V_TEXT", 60);
        DoSendKeysWhenReady("RavPayCVV_XPATH", "CVVd_TEXT", "CVV_TEXT", 60);
        DoclickWhenReady("fg_XPATH", "P_TEXT", 60);

        DoSendKeysByActionClassWhenReady( "a1_XPATH", "a11_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a2_XPATH", "a21_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady("a3_XPATH", "a31_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a4_XPATH", "a41_TEXT", "E_TEXT", 60);
    }

    public void RavePayWorkShop() throws IOException, InterruptedException {
        getDriver().switchTo().frame("checkout");

        DoSendKeysWhenReady( "RavePayCardnumber_XPATH", "Ca_TEXT", "CardNumber_TEXT", 60);
        DoSendKeysWhenReady( "RavePayValidTill_XPATH", "ValidTm_TEXT", "e1_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "RavePayValidTill_XPATH", "e2_TEXT", "V_TEXT", 60);
        DoSendKeysWhenReady("RavPayCVV_XPATH", "CVVd_TEXT", "CVV_TEXT", 60);
        DoclickWhenReady("fg_XPATH", "P_TEXT", 60);

        DoSendKeysByActionClassWhenReady( "a1_XPATH", "a11_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a2_XPATH", "a21_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a3_XPATH", "a31_TEXT", "E_TEXT", 60);
        DoSendKeysByActionClassWhenReady( "a4_XPATH", "a41_TEXT", "E_TEXT", 60);

        DoSendKeysByActionClassWhenReady("OTP_XPATH", "otp_TEXT", "otp_TEXT", 60);

        DoclickWhenReady("AuthorizePaymentBTN_XPATH", "Authorize_TEXT", 60);
    }
}
