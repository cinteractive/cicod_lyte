package CROWN.CICOD.LYTE.DeliveryRateCardSetUp;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.DosendKeysRandomNumberWhenReady;
import static CROWN.utility.Login.LoginUpgrade;
import static CROWN.utility.Utility.DoclickWhenReady;

public class UpdateDeliveryRate extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Setting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Settings_XPATH", 20);
    }

    @Description("System Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SystemSetting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SystemSettings_XPATH", 20);
    }

    @Description("Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void DeliveryRate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("DeliveryRatebtn_XPATH", 20);
    }

    @Description("Delivery Rate Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void DeliveryRateAction() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("delveryRateActionBNT_XPATH", 20);
    }

    @Description("Update Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void UpdateDeliveryRate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("UpdateDeliveryRt_XPATH", 20);
    }

    @Description("Mini Delivery Charge")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void MiniDeliveryCharge() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("MiniDeliveryCharge_XPATH", 10000, 20);
    }

    @Description("Delivery Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void DeliverySave() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("DeliverySavebtn_XPATH", 20);
    }

    @Description("Assert Delivery Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void AssertDeliverySave() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("as_XPATH","ass1_TEXT",20);
    }
}
