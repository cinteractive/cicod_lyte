package CROWN.CICOD.LYTE.DeliveryRateCardSetUp;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.Login.LoginUpgrade;
import static CROWN.utility.Utility.DoclickWhenReady;

public class DELIVERY_RATE_CARD_SetUp extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Setting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Settings_XPATH", 20);
    }

    @Description("System Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SystemSetting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SystemSettings_XPATH", 20);
    }

    @Description("Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void DeliveryRate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("DeliveryRatebtn_XPATH", 20);
    }

    @Description("Add Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void AddDeliveryRate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("DelAdd_XPATH", 20);
    }

    @Description("marchant Location")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void marchantLocation() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("marchantLocation_XPATH", 1, 20);
    }

    @Description("Select LGA")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void SelectCountry() throws IOException, InterruptedException {
        DoSelectValuesByIndex("SelectCountry_XPATH", 3, 20);
    }

    @Description("Merchant State")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void MerchantState() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("MarchantState_XPATH", 35, 20);
    }

    @Description("Merchant LGA")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void MerchantLGA() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("MarchantLGA_XPATH", 7, 20);
    }

    @Description("Mini Delivery Charge")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void MiniDeliveryCharge() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("MiniDeliveryCharge_XPATH", 10000, 20);
    }

    @Description("Percent Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void PercentDeliveryRate() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("PercentDeliveryRate_XPATH", 10, 20);
    }

    @Description("Free Delivery Price")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void FreeDeliveryPrice() throws IOException, InterruptedException {
        DoSendKeysWhenReady("FreeDeliveryPrice_XPATH", "FreeeDeliveryPrice_TEXT", 20);
    }

    @Description("Delivery Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 14)
    public void DeliverySave() throws IOException, InterruptedException {
        doclickWhenReady("DeliverySavebtn_XPATH", 20);
    }

    @Description("Assert Delivery Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 15)
    public void AssertDeliverySave() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("as_XPATH","ass_TEXT",20);
    }
}
