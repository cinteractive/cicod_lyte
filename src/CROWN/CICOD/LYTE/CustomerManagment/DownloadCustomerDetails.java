package CROWN.CICOD.LYTE.CustomerManagment;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class DownloadCustomerDetails extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Customer Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void CustomerManagement() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CustmerMGNLYTE_XPATH", 20);
    }

    @Description("Select Customer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SelectCustomer() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CustmerLyte_XPATH", 20);
    }

    @Description("Download Customer Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void DownloadCustomerDetails() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("DownloadCUstomerdbtn_XPATH", 20);
    }
}
