package CROWN.CICOD.LYTE.CustomerManagment;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import com.google.api.services.gmail.model.Thread;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.*;

public class Suspend_Unsuspend_Customer extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Customer Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void CustomerManagement() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CustmerMGNLYTE_XPATH", 20);
    }

    @Description("Select Customer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SelectCustomer() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CustmerLyte_XPATH", 20);
    }


    @Description("Action Button")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void ActionButton() throws IOException, InterruptedException, AWTException {
        DoscrolltoViewClickWhenReady("ActionSuspend_XPATH", 20);
    }

    @Description("View Customer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void ViewCustomer() throws IOException, InterruptedException, AWTException {
        DoscrolltoViewClickWhenReady("VIEWCustomer_XPATH", 20);
    }

    @Description("Suspend Customer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SuspendCustomer() throws IOException, InterruptedException, AWTException {
        DoClickWhenReadyJS("SuspendFromCustomerPage_XPATH", 20);
        acceptAlert();    }

    @Description("Unsuspend Customer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void UnsuspendCustomer() throws IOException, InterruptedException, AWTException {
        DoClickWhenReadyJS("UnsuspendfromCustomerPage_XPATH", 20);
        acceptAlert();    }
}
