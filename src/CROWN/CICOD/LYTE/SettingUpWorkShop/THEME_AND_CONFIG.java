package CROWN.CICOD.LYTE.SettingUpWorkShop;

import CROWN.Base.TestBase;
import CROWN.utility.Assertion;
import CROWN.utility.ExcelUtil;
import CROWN.utility.Login;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;

public class THEME_AND_CONFIG extends TestBase {

    protected Login login = new Login();
    protected Utility utility = new Utility();
    protected ExcelUtil excelUtil = new ExcelUtil();
    protected Assertion assertion = new Assertion();

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        login.LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        utility.DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Setting() throws IOException, InterruptedException {
        excelUtil.DoscrolltoViewClickWhenReady("Settings_XPATH", 20);
    }

    @Description("Webshop Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void WebshopSettings() throws IOException, InterruptedException {
        excelUtil.DoscrolltoViewClickWhenReady("WebshopSettings_XPATH", 20);
    }

    @Description("Update Theme")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void UpdateTheme() throws IOException, InterruptedException {
        excelUtil.DoscrolltoViewClickWhenReady("UpdateTheme_XPATH", 20);
    }

    @Description("Store Location")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void StoreLocation() throws IOException, InterruptedException {
        excelUtil.DoSelectValuesByIndex("StoreLocation_XPATH", 1, 20);
    }

    @Description("Theme Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void ThemeSave() throws IOException, InterruptedException {
        excelUtil.DoscrolltoViewClickWhenReady("ThemeSaveBTN_XPATH", 20);
    }

    @Description("Assert Theme Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void AssertThemeSave() throws IOException, InterruptedException {
        CheckElementPresent("assertThemeUpdate_XPATH", 20);
    }
}