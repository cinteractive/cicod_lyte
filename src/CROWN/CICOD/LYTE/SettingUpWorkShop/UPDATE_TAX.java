package CROWN.CICOD.LYTE.SettingUpWorkShop;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class UPDATE_TAX extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Setting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Settings_XPATH", 20);
    }

    @Description("System Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SystemSetting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SystemSettings_XPATH", 20);
    }

    @Description("Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void Tax() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Vt_XPATH", 20);
    }

    @Description("Tax Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void TaxAction() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("TaxActionbtn_XPATH", 20);
    }


    @Description("Tax Update")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void TaxUpdate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("TaxUdatebtn_XPATH", 20);
    }

    @Description("Tax Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void TaxName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("TaxName_XPATH", 20);
    }

    @Description("Tax Value")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void TaxValue() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("TaxValue_XPATH",10, 20);
    }

    @Description("Tax Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void TaxSave() throws IOException, InterruptedException {
        doclickWhenReady("TaxSavebtn_XPATH", 20);
    }

    @Description("Assert Tax Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void AssertTaxSave() throws IOException, InterruptedException {
        CheckElementPresent("AssertUpdateTax_XPATH",20);
    }
}
