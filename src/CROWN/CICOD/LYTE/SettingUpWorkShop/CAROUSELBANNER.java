package CROWN.CICOD.LYTE.SettingUpWorkShop;

import CROWN.Base.TestBase;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertEqualWhenReady;
import static CROWN.utility.ExcelUtil.DoSelectValuesByIndex;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;
import static CROWN.utility.Utility.DosendKeysRandomListwordsWhenReady;

public class CAROUSELBANNER extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("WorkShop Configuration")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void WorkShopConfiguration() throws IOException, InterruptedException {
        DoclickWhenReady("WebshopandConfLyte_XPATH", "WebshopandConfLyte_XPATH", 60);
    }

    @Description("Carousel Banner")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void CarouselBanner() throws IOException, InterruptedException {
        DoclickWhenReady("CarouselLyte_XPATH", "CarouselLyte_XPATH", 60);
    }

    @Description("Add Corusel Banner")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AddCoruselBanner() throws IOException, InterruptedException {
        DoclickWhenReady("AddCoruselBanner_XPATH", "AddCoruselBanner_XPATH", 60);
    }

    @Description("Caption")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Caption() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("Caption_XPATH", "Caption_XPATH", 60);
    }

    @Description("Display Location")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void DisplayLocation() throws IOException, InterruptedException {
        DoSelectValuesByIndex("DisplayLocation_XPATH", 1, 20);
    }

    @Description("Order Of Preference")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void OrderOfPreference() throws IOException, InterruptedException {
        DoSendKeysWhenReady("OrderOfAppearance_XPATH", "OrderOfPreference_TEXT", 20);
    }

    @Description("Action Url")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void ActionUrl() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ActiUERL_XPATH", "ActionUrl_TEXT", 20);
    }

    @Description("CAROUSEL_BANNER")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void CAROUSEL_BANNER() throws IOException, InterruptedException, AWTException {
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("CarouselSaveBtn_XPATH"))).click();
    }

    @Description("Assert CAROUSEL BANNER")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void Assert_CAROUSEL_BANNER() throws IOException, InterruptedException {
        DoAssertEqualWhenReady("AssertBanner_XPATH", "BannerAssertin_TEXT",20);
    }
}
