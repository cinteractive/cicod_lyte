package CROWN.CICOD.LYTE.SettingUpWorkShop;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;
import static CROWN.utility.Utility.acceptAlert;

public class SUSPEND_UNSUSPEND_TAX extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Setting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Settings_XPATH", 20);
    }

    @Description("System Settings")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SystemSetting() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SystemSettings_XPATH", 20);
    }

    @Description("Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void Tax() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Vt_XPATH", 20);
    }

    @Description("Tax Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void TaxAction() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("TaxActionbtn_XPATH", 20);
    }

    @Description("Suspend Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SUSPENDTax() throws IOException, InterruptedException {
        DoClickWhenReadyJS("sUSPENDTax_XPATH", 20);
        acceptAlert();
    }

    @Description("Assert Suspend Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void AssertSuspendTax() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("as_XPATH", "taxsus_TEXT",  20);
    }

    @Description("Tax Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void TaxAction1() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("TaxActionbtn_XPATH", 20);
    }

    @Description("UNSuspend Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void UNSUSPENDTax() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UnsuspendTax_XPATH", 20);
        acceptAlert();
    }

    @Description("Assert Unsuspend Tax")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void AssertUnsuspendTax() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("as_XPATH", "taxsus1_TEXT", 20);
    }
}
