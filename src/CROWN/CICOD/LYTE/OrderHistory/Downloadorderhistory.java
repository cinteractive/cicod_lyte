package CROWN.CICOD.LYTE.OrderHistory;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import CROWN.utility.Utility;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Downloadorderhistory extends TestBase {

    @Test
    public void DOWNLOAD_ORDER_HISTORY() throws IOException, InterruptedException {
        Login login = new Login();

        login.LoginTestAccount();

        //COM
        getDriver().findElement(By.xpath(Utility.fetchLocator("com_XPATH"))).click();

        getDriver().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        getDriver().findElement(By.xpath(Utility.fetchLocator("OrderHistorybtn_XPATH"))).click();

        getDriver().findElement(By.xpath(Utility.fetchLocator("DownloadOrderHistorybtn_XPATH"))).click();
    }
}