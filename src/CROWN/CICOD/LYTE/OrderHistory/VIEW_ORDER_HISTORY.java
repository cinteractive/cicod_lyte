package CROWN.CICOD.LYTE.OrderHistory;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import CROWN.utility.Utility;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.CheckElementPresent;

public class VIEW_ORDER_HISTORY extends TestBase {
    @Test
    public void VIEW_ORDER_history() throws IOException, InterruptedException {
        Login login = new Login();

        login.LoginTestAccount();

        //COM
        getDriver().findElement(By.xpath(Utility.fetchLocator("com_XPATH"))).click();

        getDriver().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        getDriver().findElement(By.xpath(Utility.fetchLocator("OrderHistorybtn_XPATH"))).click();

        getDriver().findElement(By.xpath(Utility.fetchLocator("ClickOnOrderRecordsbtn_XPATH"))).click();

        CheckElementPresent("AssertRecord_XPATH",20) ;
    }
}
