package CROWN.CICOD.LYTE.ProductManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.FileUpload.UploadFileImage3MB;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class ADD_PRODUCT extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Product Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void ProductManagement() throws IOException, InterruptedException {
        DoclickWhenReady("ProductManagementbtn_XPATH", "ProductManagementbtn_XPATH", 60);
    }

    @Description("Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Product() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("LyteProduct_XPATH", 20);
    }

    @Description("Add Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AddProduct() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Add_Producttn_XPATH", 20);
    }

    @Description("Select Product Category")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void SelectProductCategory() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("SelectProductCategory_XPATH", 7, 20);
    }

    @Description("Product Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void ProductName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("ProductNamei_XPATH", 20);
    }

    @Description("Product Description")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void ProductDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ProductDiscriptioni_XPATH", "ProductDiscription_TEXT", 20);
    }

    @Description("Product Code")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void ProductCode() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomNumberWhenReady("ProductCode_XPATH", 100000, 20);
    }

    @Description("Product Prize")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void ProductPrize() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomNumberWhenReady("ProductPrize_XPATH", 100000, 20);
    }

    @Description("Upload Product IMG")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void ProductDetails() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("AddToworkshoptCheckBox_XPATH", 10);
        DosendKeysRandomNumberWhenReady("ReservationDays_XPATH", 100000, 20);
        doclickWhenReady("NoQuantityLimitCheckBox_XPATH", 10);
        DoSelectValuesByIndex("Vat_XPATH", 1, 20);
    }

    @Description("Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void Save() throws IOException, InterruptedException, AWTException {
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("isave_XPATH"))).click();
    }

    @Description("Assert ADD PRODUCT")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 14)
    public void AssertADDPRODUCT() throws IOException, InterruptedException, AWTException {
        DoAssertContainsWhenReady("AssertProductCreation_XPATH", "assUpdates_TEXT", 20);
    }
}