package CROWN.CICOD.LYTE.ProductManagement;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.*;

public class SUSPEND_UNSUSPEND_PRODUCTS extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Product Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void ProductManagement() throws IOException, InterruptedException {
        DoclickWhenReady("ProductManagementbtn_XPATH", "ProductManagementbtn_XPATH", 60);
    }

    @Description("Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Product() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("LyteProduct_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("ActionSuspendbtn_XPATH", 20);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Suspendbtn_XPATH", 20);
        acceptAlert();
        TimeUnit.SECONDS.sleep(2);
    }

    @Description("AssertSuspend")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void AssertSuspend() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Assertsusp_XPATH","suscont_TEXT",20);
    }

    @Description("Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void Action2() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("ActionSuspendbtn_XPATH", 20);
    }

    @Description("Unsuspend")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void Unsuspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Suspendbtn_XPATH", 20);
        acceptAlert();
    }

    @Description("AssertSuspend")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void AssertUnsuspend() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Assertsusp_XPATH","suscont1_TEXT",20);
    }
}
