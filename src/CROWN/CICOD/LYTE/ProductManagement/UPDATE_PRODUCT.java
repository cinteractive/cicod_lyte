package CROWN.CICOD.LYTE.ProductManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import CROWN.utility.Utility;
import com.google.api.services.gmail.model.Thread;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.Utility.DoclickWhenReady;

public class UPDATE_PRODUCT extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Product Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void ProductManagement() throws IOException, InterruptedException {
        DoclickWhenReady("ProductManagementbtn_XPATH", "ProductManagementbtn_XPATH", 60);
    }

    @Description("Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Product() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("LyteProduct_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("ActionSuspendbtn_XPATH", 20);
    }

    @Description("Update")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Updatebtn_XPATH", 20);
    }

    @Description("Product Description")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void ProductDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ProductDiscriptioni_XPATH", "ProductDiscription_TEXT", 20);
    }

    @Description("Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void Save() throws IOException, InterruptedException, AWTException {
        getDriver().findElement(By.xpath(Utility.fetchLocator("isave_XPATH"))).click();
    }

    @Description("Assert UPDATE PRODUCT")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void AssertUpdateProduct() throws IOException, InterruptedException{
        DoAssertContainsWhenReady("AssertProductCreation_XPATH", "updateProduct_TEXT", 20);
    }
}