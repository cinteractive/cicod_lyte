package CROWN.CICOD.LYTE.ProductManagement;

import CROWN.Base.TestBase;
import CROWN.utility.FileUpload;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.FileUpload.UploadFileImage3MB;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class PRODUCT_CATEGORY extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Product Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void ProductManagement() throws IOException, InterruptedException {
        DoclickWhenReady("ProductManagementbtn_XPATH", "ProductManagementbtn_XPATH", 60);
    }

    @Description("Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Product() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("LyteProductCate_XPATH", 20);
    }

    @Description("Add Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AddProduct() throws IOException, InterruptedException {
        DoclickWhenReady("AddProductbtn_XPATH", "AddProductbtn_XPATH", 60);
    }

    @Description("Product Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void ProductName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("ProductName_XPATH", 20);
    }

    @Description("Product Discription")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void ProductDiscription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ProductDiscription_XPATH", "ProductDiscription_TEXT", 20);
    }

    @Description("Save")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void Save() throws IOException, InterruptedException {
        doclickWhenReady("savvbtn_XPATH", 20);
    }

    @Description("Assert Create Product Category")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void AssertProductCat() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertProductCate_XPATH", "catsacant_TEXT", 20);
    }
}