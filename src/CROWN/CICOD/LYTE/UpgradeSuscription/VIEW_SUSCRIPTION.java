package CROWN.CICOD.LYTE.UpgradeSuscription;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.Login.LoginUpgrade;

public class VIEW_SUSCRIPTION extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Billing() throws IOException, InterruptedException {
        doclickWhenReady("sdsd_XPATH", 20);
        doclickWhenReady("SuscriptionBTN_XPATH", 20);

    }

    @Description("Assert Subscription Page")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertSubscriptionPage() throws IOException, InterruptedException {
        CheckElementPresent("SuscriptionPage_XPATH",20);
    }

    @Description("Assert Billing Amount")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AssertBillingAmount() throws IOException, InterruptedException {
        CheckElementPresent("AssertBillAmount_XPATH",20);
    }
}
