package CROWN.CICOD.LYTE.UpgradeSuscription;

import CROWN.Base.TestBase;
import CROWN.utility.Assertion;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;

public class Pay_By_Bank_Transfer extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Billing() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Billing1_XPATH", 20);
    }

    @Description("Subscription")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Subscription() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SuscriptionBTN_XPATH", 20);
    }

    @Description("Upgrade")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Upgrade() throws IOException, InterruptedException {
        doclickWhenReady("Upgrade_XPATH", 20);
    }

    @Description("Upgrade to Premium")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void UpgradedPremium() throws IOException, InterruptedException {
        doclickWhenReady("UpgradetoPremium_XPATH", 20);
    }

    @Description("Pay now")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Paynow() throws IOException, InterruptedException {
        doclickWhenReady("Paynow1_XPATH", 20);
    }

    @Description("Pay by Transfer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void PaybyTransfer() throws IOException, InterruptedException {
        doclickWhenReady("PaybyTransfer_XPATH", 20);
    }

    @Description("Providous Bank")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void providousbank() throws IOException, InterruptedException {
        doclickWhenReady("providousbank_XPATH", 20);
    }

    @Description("Assert Offer Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void AssertOfferDetails() throws IOException, InterruptedException {
        Assertion.CheckElementPresent("bankt_XPATH", 20);
    }

    @Description("Navigate Back")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void NavigateBack() throws IOException, InterruptedException {
        doclickWhenReady("BackNT_XPATH", 20);
    }

    @Description("Bank Transfer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void BankTransfer() throws IOException, InterruptedException {
        doclickWhenReady("PaybyTransfer_XPATH", 20);
        doclickWhenReady("closebtn_XPATH", 20);
    }

    @Description("Assert Pay By Transfer")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void AssertPayByTransfer() throws IOException, InterruptedException {
        CheckElementPresent("PaybyTransfer_XPATH",20);
    }
}
