package CROWN.CICOD.LYTE.UpgradeSuscription;

import CROWN.Base.TestBase;
import com.google.api.services.gmail.model.Thread;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginUpgrade;
import static CROWN.utility.RavePay.RavePay1;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;
import static CROWN.utility.Utility.acceptAlert;

public class FailedSubscriptionRenewal extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Billing() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Billing1_XPATH",20);
    }

    @Description("Subscription")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Subscription() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SuscriptionBTN_XPATH",20);
    }

    @Description("Upgrade")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Upgrade() throws IOException, InterruptedException {
        doclickWhenReady("Upgrade_XPATH",20);
    }

    @Description("Upgrade to Premium")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void UpgradedPremium() throws IOException, InterruptedException {
        doclickWhenReady("UpgradetoPremium_XPATH",20);
    }

    @Description("Pay now")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Paynow() throws IOException, InterruptedException {
        doclickWhenReady("Paynow_XPATH",20);
    }

    @Description("SubPay On line")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SubPayOnline() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SubPayOnline_XPATH",20);
    }

    @Description("Rave Pay")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void RavePay() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(3);
        RavePay1();
    }

    @Description("Close Fluter Wave")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void CloseFluterWave() throws IOException, InterruptedException {
       DoClickWhenReadyJS("CloseFluterWave_XPATH",20);
    }

    @Description("Close Fluter Wave")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void FAILED_SUBSCRIPTION_RENEWAL(){
        acceptAlert();

        getDriver().switchTo().defaultContent();
        (new WebDriverWait(getDriver(), 45)).until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div/section/div/div/div/div[2]/div/div/a")));
    }
}
