package CROWN.CICOD.LYTE.UpgradeSuscription;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginUpgrade;
import static CROWN.utility.RavePay.RavePay1;

public class UpgradeSuscriptionOnFreeTrial extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Billing() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Billing1_XPATH", 2);
    }

    @Description("Subscription")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Subscription() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SuscriptionBTN_XPATH", 20);
    }

    @Description("Upgrade")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Upgrade() throws IOException, InterruptedException {
        doclickWhenReady("Upgrade_XPATH", 20);
    }

    @Description("Upgrade to Premium")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void UpgradedPremium() throws IOException, InterruptedException {
        doclickWhenReady("UpgradetoPremium_XPATH", 20);
    }

    @Description("Pay now")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Paynow() throws IOException, InterruptedException {
        doclickWhenReady("Paynow_XPATH", 20);
    }

    @Description("SubPay On line")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SubPayOnline() throws IOException, InterruptedException {
        doclickWhenReady("SubPayOnline_XPATH", 20);
    }

    @Description("Rave Pay")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void RavePay() throws IOException, InterruptedException {
        RavePay1();
    }

    @Description("Assert Up grade")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void AssertUpgrade() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Assssss_XPATH","mk_TEXT", 20);
    }
}
