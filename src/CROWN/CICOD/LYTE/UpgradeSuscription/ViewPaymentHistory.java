package CROWN.CICOD.LYTE.UpgradeSuscription;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginUpgrade;

public class ViewPaymentHistory extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Billing() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Billing1_XPATH", 2);
        doclickWhenReady("BillandPaymentHistory_XPATH", 20);

    }

    @Description("Assert Subscription Page")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertSubscriptionPage() throws IOException, InterruptedException {
        CheckElementPresent("AssertBillandPaymentHistoryPage_XPATH",20);
    }
}
