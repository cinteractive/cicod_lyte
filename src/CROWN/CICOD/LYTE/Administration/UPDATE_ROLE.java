package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ClickAll.clickall;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class UPDATE_ROLE extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 20);
    }

    @Description("Role Management Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RoleManagementButton() throws IOException, InterruptedException {
        doclickWhenReady("RoleLyte_XPATH", 20);
    }

    @Description("Update Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void UpdateRole() throws IOException, InterruptedException {
        doclickWhenReady("RoleActionbtn_XPATH", 20);
        getDriver().findElement(By.linkText("Update")).click();
    }

    @Description("Click All Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void ClickAllRole() throws IOException, InterruptedException, AWTException {
        clickall("aa_CLASS");
    }

    @Description("Save Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void SaveRole() throws IOException, InterruptedException, AWTException {
        DoscrolltoViewClickWhenReady("lyteAddrole_XPATH", 20);
    }

    @Description("Assert Unsuspend Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AssertUpdateRole() throws IOException, InterruptedException, AWTException {
        CheckElementPresent("assertUpdateRole_XPATH", 20);
    }
}
