package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ClickAll.clickall;
import static CROWN.utility.ExcelUtil.DosendKeysRandomListwordsWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.JavaScriptUtil.DoZoomPercentage;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class ADD_ROLE extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 20);
    }

    @Description("Role Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RoleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RoleLyte_XPATH", 20);
    }

    @Description("Add Role Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void AddRoleButton() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AddRolebtn_XPATH", 20);
    }

    @Description("Role Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void RoleName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("RoleName_XPATH", 20);
    }

    @Description("Click All")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void ClickAll() throws IOException, InterruptedException, AWTException, AWTException {
        DoZoomPercentage(20);
        Thread.sleep(5000);
        clickall("aa_CLASS");
    }

    @Description("Add Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AddButton() throws IOException, InterruptedException, AWTException {
        DoClickWhenReadyJS("lyteAddrole_XPATH", 20);
    }

    @Description("Assert Add Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertAddRole() throws IOException, InterruptedException, AWTException {
        DoAssertContainsWhenReady("AlertRoleSSS_XPATH","q1_TEXT",20);
    }
}
