package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class DOWNLOAD_User extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 60);

    }

    @Description("User Management Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void UserManagementButton() throws IOException, InterruptedException {
        DoclickWhenReady("UserManagementLYTE_XPATH", "UserManagementLYTE_XPATH", 60);
    }

    @Description("DOWNLOAD USER")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void DOWNLOAD_USER() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UserDownload_XPATH", 20);
    }
}
