package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.DoclickWhenReady;

public class AddUser extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 60);

    }

    @Description("User Management Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void UserManagementButton() throws IOException, InterruptedException {
        DoclickWhenReady("UserManagementLYTE_XPATH", "UserManagementLYTE_XPATH", 60);
    }

    @Description("AddUser Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void AddUserButton() throws IOException, InterruptedException {
        DoclickWhenReady("AddUserbtn_XPATH", "AddUserbtn_XPATH", 20);
    }

    @Description("Selecting Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void SelectingRole() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Role_XPATH", 1, 20);
    }

    @Description("Selecting Region")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void SelectingRegion() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Region_XPATH", 1, 20);
    }

    @Description("First Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void FirstName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("RFirsteName_XPATH", 20);
    }

    @Description("Last Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void LastName() throws IOException, InterruptedException {
        DoSendKeysWhenReady("RLastName_XPATH", "CustomerLastName_TEXT", 20);
    }

    @Description("Phone Number")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void PhoneNumber() throws IOException, InterruptedException {
        DoSendKeysWhenReady("RPhoneNumber_XPATH", "CustomerPhoneNumber_TEXT", 20);
    }

    @Description("Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Email() throws IOException, InterruptedException {
        DosendKeysRandomEmailsWhenReady("REmail_XPATH", 20);
    }

    @Description("ADD USER")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void ADD_USER() throws IOException, InterruptedException {
        doclickWhenReady("CreateUserSaveBTN_XPATH", 20);
    }

    @Description("Assert Add User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void AssertAddUser() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertAddUser_XPATH", "containUser_TEXT", 20);
    }
}
