package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;
import static CROWN.utility.Utility.acceptAlert;

public class SUSPEND_UNSUSPEND_ROLE extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 20);
    }

    @Description("Role Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RoleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RoleLyte_XPATH", 20);
    }

    @Description("Suspend Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void SuspendRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RoleActionbtn_XPATH", 20);
        DoClickWhenReadyJS("suspend/unsuspendRole_XPATH", 20);
        acceptAlert();
    }


    @Description("Unsuspend Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void UnsuspendRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RoleActionbtn_XPATH", 20);
        DoClickWhenReadyJS("suspend/unsuspendRole_XPATH", 20);
        acceptAlert();
    }
}
