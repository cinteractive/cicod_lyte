package CROWN.CICOD.LYTE.Administration;

import CROWN.Base.TestBase;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginUpgrade;
import static CROWN.utility.Utility.DoclickWhenReady;

public class UpdateUser extends TestBase {

    @Description("Login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginUpgrade();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Administration Module")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void AdministrationModule() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AdminLyte_XPATH", 60);

    }

    @Description("User Management Button")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void UserManagementButton() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("UserManagementLYTE_XPATH",60);
    }

    @Description("Update User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void UpdateUser() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UserAction_XPATH", 20);
        getDriver().findElement(By.linkText("Update")).click();
    }


    @Description("Update First name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void UpdateFirstname() throws IOException, InterruptedException {
        Thread.sleep(2000);
        WebElement ty = getDriver().findElement(By.xpath(Utility.fetchLocator("RFirsteName_XPATH")));
        ty.clear();
        ty.sendKeys(Utility.fetchLocator("CustomerFirstname_TEXT"));
    }

    @Description("Update Last name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void UpdateLastname() throws IOException, InterruptedException {
        Thread.sleep(1000);
        WebElement tom = getDriver().findElement(By.xpath(Utility.fetchLocator("RLastName_XPATH")));
        tom.clear();
        tom.sendKeys(Utility.fetchLocator("CustomerLastName_TEXT"));
    }

    @Description("Update Phone Number")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void UpdatePhoneNumber() throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebElement top = getDriver().findElement(By.xpath(Utility.fetchLocator("RPhoneNumber_XPATH")));
        top.clear();
        top.sendKeys(Utility.fetchLocator("CustomerPhoneNumber_TEXT"));
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Update() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CreateUserSaveBTN_XPATH", 20);
    }

    @Description("Assert Update User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void AssertUpdateUser() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("yyggg_XPATH", "wer_TEXT", 20);
    }
}
