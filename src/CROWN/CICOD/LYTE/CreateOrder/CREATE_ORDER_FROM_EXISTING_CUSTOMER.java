package CROWN.CICOD.LYTE.CreateOrder;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.RavePay.RavePay2;
import static CROWN.utility.Utility.DoclickWhenReady;

public class CREATE_ORDER_FROM_EXISTING_CUSTOMER extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginTestAccount();
    }

    @Description("Customer Order Management")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Create Order")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void CreateOrder() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Createorderbtn_XPATH", 30);
    }

    @Description("Search Customer By Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SearchCustomerByName() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SearchByName_XPATH", 30);
        DoSendKeysWhenReady("SeaerchInput_XPATH", "CustomerFirstname_TEXT", 20);
        DoscrolltoViewClickWhenReady("Searchbtn_XPATH", 30);
    }

    @Description("View Customer Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void ViewCustomerDetails() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("ViewDetails_XPATH", 30);
    }

    @Description("Assert View Customer Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void AssertViewCustomerDetails() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertSearchByName_XPATH", "cot_TEXT", 20);
    }

    @Description("Search Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SearchProduct() throws IOException, InterruptedException {
        DoSendKeysWhenReady("SearchProductinput_XPATH", "SearchedProduct_TEXT", 20);
        DoscrolltoViewClickWhenReady("SearchProductbtnq_XPATH", 30);
    }

    @Description("Add Product to Chart")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void AddProducttoChart() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Addbtn_XPATH", 30);
    }

    @Description("Apply Discount")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void ApplyDiscount() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Applydiscount_XPATH", 30);
        DoSendKeysWhenReady("DiscountByPercent_XPATH", "10_TEXT", 30);
        DoscrolltoViewClickWhenReady("OkDiscount_XPATH", 30);
        DoscrolltoViewClickWhenReady("ConfirmOKDiscount_XPATH", 30);
    }

    @Description("Make Payment")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void MakePayment() throws IOException, InterruptedException {
        DoClickWhenReadyJS("MakePayment_XPATH", 30);
        DoClickWhenReadyJS("PayOnline_XPATH", 30);
    }

    @Description("Rav Pay")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void RavPay() throws IOException, InterruptedException {
        RavePay2();
    }

    @Description("Assert Pay Online")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void AssertPayOnline() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Auth_XPATH", "rtu_TEXT", 20);
    }
}
