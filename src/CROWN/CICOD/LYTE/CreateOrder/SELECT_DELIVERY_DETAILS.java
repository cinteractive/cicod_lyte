package CROWN.CICOD.LYTE.CreateOrder;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginTestAccount;
import static CROWN.utility.Utility.DoclickWhenReady;

public class SELECT_DELIVERY_DETAILS extends TestBase {

    @Description("Login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws Exception {
        LoginTestAccount();
    }

    @Description("COM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void CustomerOrderManagement() throws IOException, InterruptedException {
        DoclickWhenReady("com_XPATH", "comm_TEXT", 60);
    }

    @Description("Test Create Order")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void CreateOrder() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Createorderbtn_XPATH", 30);
    }

    @Description("Search Customer By Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void SearchCustomerByName() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("SearchByName_XPATH", 30);
        DoSendKeysWhenReady("SeaerchInput_XPATH", "CustomerFirstname_TEXT", 20);
        DoscrolltoViewClickWhenReady("Searchbtn_XPATH", 30);
    }

    @Description("View Customer By Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void ViewCustomerDetails() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("ViewDetails_XPATH", 30);
    }

    @Description("Assert View Customer Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void AssertViewCustomerDetails() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertSearchByName_XPATH", "cot_TEXT", 20);
    }

    @Description("Search Product By Name")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void SearchProduct() throws IOException, InterruptedException {
        DoSendKeysWhenReady("SearchProductinput_XPATH", "SearchedProduct_TEXT", 20);
        DoscrolltoViewClickWhenReady("SearchProductbtnq_XPATH", 30);
    }

    @Description("Add Product to Chart")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void AddProducttoChart() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Addbtn_XPATH", 30);
        DoscrolltoViewClickWhenReady("Addbtn_XPATH", 30);
    }

    @Description("Delivery Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void DeliveryRate() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("DeliveryDetails_XPATH", 30);
    }

    @Description("Add Rate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void AddRate() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        DoClickWhenReadyJS("AddDeliveryRate_XPATH", 30);
        DoClickWhenReadyJS("AddAdd_XPATH", 30);
    }

    @Description("Delivery Details")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void DeliveryDetails() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("DliveryName_XPATH", 30);
        DosendKeysRandomListwordsWhenReady("DeliveryStreet_XPATH", 30);
        DosendKeysRandomListwordsWhenReady("DeliveryLandMark_XPATH", 30);
    }

    @Description("LGA")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void Country() throws IOException, InterruptedException {
        DoSelectValuesByIndex("DeliveryCountry_XPATH", 3, 20);
    }

    @Description("LGA")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void State() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("DeliveryState_XPATH", 36, 20);
    }

    @Description("Add Product to Chart")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 15)
    public void LGA() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("DeliveryLGA_XPATH", 2, 20);
    }

    @Description("Save Delivery Address")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 16)
    public void SaveDeliveryAddress() throws IOException, InterruptedException {
        doclickWhenReady("SAveDeiveryAddress_XPATH", 20);
    }

    @Description("Assert Save Delivery")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 17)
    public void AssertSaveDelivery() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("as_XPATH", "CotnTest_TEXT", 20);
    }
}
